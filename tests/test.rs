use std::panic::catch_unwind;

use symbol_table::static_symbol;
use tensoratu::{Index, IndexAtom, IndexElem, IndexSeq};
use tensoratu::{index_atom, index, indices};

#[test]
fn indices() {
    let ind = indices![foo bar, hey];
    let ind2 = indices![foo bar, hey];
    assert!(ind == ind2);
}

#[test]
fn index_macros() {
    assert!(catch_unwind(|| IndexAtom::new("a", Some(0))).is_err());
    let s = static_symbol!(" Some Symbol ");
    assert_eq!(
        IndexAtom::new(" Some Symbol ", Some(2)),
        IndexAtom::from_symbol(s, Some(2)),
    );

    let a = index_atom!(a);
    assert_eq!(a, IndexAtom::new("a", None));

    let dyna = vec![
        IndexElem { len: 2 },
        IndexElem { atom: index_atom!(a) },
        IndexElem { atom: index_atom!(b(5)) },
        IndexElem { len: 1 },
        IndexElem { atom: index_atom!(c) },
    ];
    let dyna = unsafe { IndexSeq::new_unchecked(dyna) };
    let stat = indices![a b(5), c];
    assert!(dyna == stat);

    let indices: Vec<&Index> = dyna.iter().collect();
    assert!(indices[0] == index!(a b(5)));
    assert!(indices[1] == index!(c));
}
