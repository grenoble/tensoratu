mod index;
mod macros;

pub use index::{Index, IndexAtom, IndexElem, IndexSeq};
