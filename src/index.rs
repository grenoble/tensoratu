#![allow(dead_code)]
use std::num::{NonZero, NonZeroU32};
use std::ops::Deref;

use symbol_table::GlobalSymbol;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct IndexAtom {
    symbol: GlobalSymbol,
    dim: Option<NonZeroU32>,
}

pub type Index = [IndexAtom];

#[doc(hidden)]
#[derive(Copy, Clone)]
pub union IndexElem {
    pub atom: IndexAtom,
    pub len: usize,
}

// Ensure that transmuting &[IndexElem] to &[IndexAtom] is sound.
const _: () = assert!(
    size_of::<IndexElem>() == size_of::<IndexAtom>(),
    "IndexElem and IndexAtom must have the same size."
);
const _: () = assert!(
    align_of::<IndexElem>() >= align_of::<IndexAtom>(),
    "The alignment of IndexElem must be compatible with IndexAtom."
);

/// A sequence of indices.
///
/// Technically, this is a [custom dynamically sized type
/// (DST)](https://doc.rust-lang.org/nomicon/exotic-sizes.html#dynamically-sized-types-dsts).
// Ensure that transmuting &[IndexElem] to &IndexSeq<[IndexElem]> is sound.  See
// https://users.rust-lang.org/t/emulating-array-slice-vec-in-a-library-type/126442/2
#[repr(transparent)]
pub struct IndexSeq<T>(T)
where
    T: ?Sized,
    T: AsRef<[IndexElem]>;

impl IndexAtom {
    pub fn new(name: &str, dim: Option<u32>) -> Self {
        IndexAtom::from_symbol(GlobalSymbol::new(name), dim)
    }

    pub fn from_symbol(symbol: GlobalSymbol, dim: Option<u32>) -> Self {
        match dim {
            Some(dim) => {
                assert!(dim > 0, "Dimension of index must not be zero.");
                IndexAtom {
                    symbol,
                    // NonZero::new returns Option<NonZero>, so we can use this directly.
                    dim: NonZero::new(dim),
                }
            }
            None => IndexAtom { symbol, dim: None },
        }
    }

    pub fn dim(&self) -> Option<usize> {
        self.dim.map(|dim| dim.get() as usize)
    }

    pub fn symbol(&self) -> GlobalSymbol {
        self.symbol
    }
}

impl<T> IndexSeq<T>
where
    T: AsRef<[IndexElem]>,
{
    /// Creates a new index sequence from the provided elements, without any checking.
    ///
    /// It is normally not necessary to call this function directly.  Please
    /// use the macro [indices](crate::indices) instead.
    ///
    /// # Safety
    ///
    /// the provided elements must be the concatenation of an arbitrary number of blocks,
    /// where each block has the following form:
    ///
    /// One `IndexElem { len: N }` followed by N times `IndexElem { atom: ... }`.
    #[inline]
    pub unsafe fn new_unchecked(elements: T) -> Self {
        Self(elements)
    }
}

impl<T> IndexSeq<T>
where
    T: ?Sized,
    T: AsRef<[IndexElem]>,
{
    pub fn iter(&self) -> impl Iterator<Item = &Index> {
        IndexSeqIterator {
            data: self.0.as_ref(),
            idx: 0,
        }
    }
}

impl<T> Deref for IndexSeq<T>
where
    T: AsRef<[IndexElem]>,
{
    type Target = IndexSeq<[IndexElem]>;

    fn deref(&self) -> &Self::Target {
        let data = self.0.as_ref();
        // SAFETY: This relies on IndexSeq having #[repr(transparent)].
        unsafe { &*(data as *const [IndexElem] as *const IndexSeq<[IndexElem]>) }
    }
}

// MAYBE: Reimplement using pointers, like std::slice::Iter.
struct IndexSeqIterator<'a> {
    data: &'a [IndexElem],
    // SAFETY: Must always point to a length element.
    idx: usize,
}

impl<'a> Iterator for IndexSeqIterator<'a> {
    type Item = &'a Index;

    fn next(&mut self) -> Option<&'a Index> {
        let idx = self.idx;
        if idx == self.data.len() {
            return None;
        }
        let len = &self.data[idx];
        // SAFETY: idx always points to a length element.
        let len = unsafe { len.len };
        self.idx = idx + len + 1;
        let elem_slice = &self.data[(idx + 1)..self.idx];
        // SAFETY: This relies on IndexElem having size and alignment compatible with IndexAtom.
        // This is checked with const assertions following those types’ definitions.
        Some(unsafe { &*(elem_slice as *const [IndexElem] as *const [IndexAtom]) })
    }
}

impl<A, B> PartialEq<IndexSeq<B>> for IndexSeq<A>
where
    A: ?Sized + AsRef<[IndexElem]>,
    B: ?Sized + AsRef<[IndexElem]>,
{
    fn eq(&self, other: &IndexSeq<B>) -> bool {
        // If the underlying slices do not have the same length
        if self.0.as_ref().len() != other.0.as_ref().len() { return false; }
        let mut a = self.iter();
        let mut b = other.iter();
        loop {
            match (a.next(), b.next()) {
                (Some(a), Some(b)) => {
                    if a != b {
                        return false;
                    }
                }
                (None, None) => {
                    return true;
                }
                _ => {
                    return false;
                }
            }
        }
    }
}

impl<T> Eq for IndexSeq<T> where T: AsRef<[IndexElem]> {}

pub fn print(indices: &IndexSeq<[IndexElem]>) {
    for index in indices.iter() {
        for atom in index {
            print!("{}, ", atom.symbol);
        }
        println!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    const fn index_atom_size() {
        // Check that index atoms are at most pointer-sized on 64 bit architectures.
        assert!(size_of::<usize>() != 8 || size_of::<IndexAtom>() <= size_of::<usize>());
    }

    #[test]
    fn index_atom() {
        let foo = IndexAtom::new("foo", None);
        let bar = IndexAtom::new("bar", Some(4));
        assert!([foo, bar] == [crate::index_atom!(foo), crate::index_atom!(bar(2+2))]);
        assert!([foo, bar] == crate::index!(foo bar(4)));
    }
}
