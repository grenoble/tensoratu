// Based on
// https://veykril.github.io/tlborm/decl-macros/building-blocks/counting.html#bit-twiddling
#[doc(hidden)]
#[macro_export]
macro_rules! _count_exprs {
    () => { 0 };
    ($single:expr) => { 1 };
    ($odd:expr, $($a:expr, $b:expr),*) => { ($crate::_count_exprs!($($a),*) << 1) | 1 };
    ($($a:expr, $even:expr),*) => { $crate::_count_exprs!($($a),*) << 1 };
}

// Idea: symbol_table::static_symbol macro
#[macro_export]
macro_rules! index_atom {
    ($symb:ident) => {{
        use std::sync::OnceLock;
        static SYMBOL: OnceLock<symbol_table::GlobalSymbol> = OnceLock::new();
        let symbol = SYMBOL.get_or_init(|| symbol_table::GlobalSymbol::from(stringify!($symb)));
        $crate::IndexAtom::from_symbol(*symbol, None)
    }};
    ($symb:ident ($dim:expr)) => {{
        use std::sync::OnceLock;
        static SYMBOL: OnceLock<symbol_table::GlobalSymbol> = OnceLock::new();
        let symbol = SYMBOL.get_or_init(|| symbol_table::GlobalSymbol::from(stringify!($symb)));
        $crate::IndexAtom::from_symbol(*symbol, Some($dim))
    }};
}

#[macro_export]
macro_rules! index {
    ($($symb:ident $(($dim:expr))?)*) => { [ $($crate::index_atom!($symb $(($dim))?)),* ] }
}

#[macro_export]
macro_rules! indices {
    ($($($symb:ident $(($dim:expr))?)*),*) => {{
        let elements = [
            $(
                $crate::IndexElem { len: $crate::_count_exprs!($($symb),*) },
                $( $crate::IndexElem { atom: $crate::index_atom!($symb $(($dim))?) } ),*
            ),*
        ];
        // SAFETY: The use of _count_exprs assures that the array of elements has the required
        // structure.
        unsafe { $crate::IndexSeq::new_unchecked(elements) }
    }}
}
