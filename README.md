# Tensoratu: a symphony of indexing

Tensoratu sets out to end the horror of indexing when working with tensor networks.
As a Rust library, Tensoratu aims to be idiomatic and efficient.
Python bindings are planned.

For a technical overview see the documents on software [design](DESIGN.md) and [architecture](ARCHTECTURE.md).

## License
Dual-licensed to be compatible with the Rust project.
See the [license statement](LICENSE.md).
